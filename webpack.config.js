const path = require('path');
const webpack = require('webpack');

module.exports = (env, argv) => {
    const isProd = argv.mode === 'production';
    return {
        entry: {
            calc: './src/index.js',
        },
        output: {
            filename: isProd ? '[name].umd.min.js' : '[name].umd.js',
            path: path.resolve(__dirname, 'dist'),
            publicPath: '/assets/',
            libraryTarget: 'umd',
            library: 'calc',
        },
        module: {
            rules: [
                {
                    test: /\.js$/,
                    exclude: /node_modules/,
                    loader: 'babel-loader',
                },
            ],
        },
        devServer: {
            contentBase: path.join(__dirname, 'test'),
            compress: true,
            hot: true,
            port: 9000,
        },
        plugins: [new webpack.HotModuleReplacementPlugin()],
    };
};
