import { add, sub, mul, div } from './calc';

module.exports = {
    add,
    sub,
    mul,
    div,
};

if (module && module.hot) module.hot.accept();
